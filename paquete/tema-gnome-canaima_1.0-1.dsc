Format: 3.0 (quilt)
Source: tema-gnome-canaima
Binary: tema-gnome-canaima
Architecture: i386
Version: 1.0-1
Maintainer: Andres Moreno, Rebeca Ochoa, Josè Goncalves <canaima@localhost>
Homepage: <insert the upstream URL, if relevant>
Standards-Version: 3.9.8
Build-Depends: debhelper (>= 9)
Package-List:
 tema-gnome-canaima deb misc optional arch=i386
Checksums-Sha1:
 781f07a701c00e394d55e951e2539bb6d004c8d3 3223775 tema-gnome-canaima_1.0.orig.tar.gz
 a4f3c1b739895ec62734a0e7aa9af4b1f0726e96 5624 tema-gnome-canaima_1.0-1.debian.tar.xz
Checksums-Sha256:
 76def0ebc8a3791afcc57f65f98c3ed5938649169c9540eadb71651eb58feb41 3223775 tema-gnome-canaima_1.0.orig.tar.gz
 b51745a0b91a2744c3fa86b8fc4fb62b39423cfa314468b3b8dafe4bafbb1d2c 5624 tema-gnome-canaima_1.0-1.debian.tar.xz
Files:
 3502e296a832f04181451b4dca04f1b9 3223775 tema-gnome-canaima_1.0.orig.tar.gz
 003eb0705458e1c3907efb4bc70f3b22 5624 tema-gnome-canaima_1.0-1.debian.tar.xz
